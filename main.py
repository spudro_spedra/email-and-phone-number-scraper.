import urllib.request, os.path, csv, re, googlesearch, time
from urllib.parse import quote
from bs4 import BeautifulSoup

city = "kraków" # make that optional
path_of_folder = os.path.dirname(os.path.realpath(__file__)) + '/data/'

def get_search_name():
	"""
	Get a word that will be entered in the google search.
	"""

	got = False

	# TODO: add custom search num after a comma. or pipes - ... |warszawa|150

	while not got:
		search = input("What type of company are you searching for? >")

		goode_yes_no = input("Searching for: '{0}' - is that ok? (Y) (N) (Q) >".format(search + " " + city))

		if goode_yes_no.lower() == 'y':
			print("Searching google...")
			return search

		elif goode_yes_no.lower() == 'n':
			print("Ok, let's start again:")
			continue

		elif goode_yes_no.lower() == 'q':
			exit(0)

		else:
			print("y or n or q only")
			continue

def get_html_from_url(url):

	headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:57.0) Gecko/20100101 Firefox/57.0',
				'Accept': "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
       			'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       			'Accept-Encoding': 'none',
       			'Accept-Language': 'pl-PL,pl;q=0.8'}

	req = urllib.request.Request(url, headers=headers)

	try:
		with urllib.request.urlopen(req, timeout=10) as google_binary:
			html = google_binary.read().decode('utf-8')
			return html
	except Exception as e:
		if "404" in str(e):
			return "404"
		elif "403" in str(e):
			pass
		elif "utf" in str(e):
			pass
		elif "timed" in str(e):
			pass
		elif "certificate" in str(e):
			pass
		elif "999" in str(e):
			pass
		elif "500" in str(e):
			return ""
		else:
			with open(path_of_folder + "crash.txt", 'w') as file:
				file.write(str(e))
				file.write("\n")
				file.write(url)
				file.close()
			#print("Something went wrong:")
			#print(str(e))
			#print(url)
			exit(0)

def simple_get_links(search_string, number_of_search_results):

	return [link for link in googlesearch.search(search_string,
								tld="pl",
								lang="pl",
								num=number_of_search_results,
								pause=2.0,
								stop=number_of_search_results,
								user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:57.0) Gecko/20100101 Firefox/57.0'
								) if not ("youtube" in link or "facebook" in link or "olx" in link or "allegro" in link or "sprzedajemy" in link or "gumtree" in link)]

def parse_site(soup, url):
	"""
	Parse soup from retrieved html from get_html_from_url and soupified.
	"""

	found = {'emails': False,
			'phones': False}

	try_to_find = True
	added_kontakt = False
	added_html = False
	searched_through_a_tag = False

	first_url = url

	counter = 0
	while try_to_find:

		mail_regex = r'\b[\w^.]+@\w+[.]\w+[.com|.pl|.eu|.org]+'
		phone_regex = r'\d{3}\s\d{3}\s\d{3}|[+]48\s12\s\d{3}\s\d{3}|012\s\d{3}\s\d{2}\s\d{2}|[+]48\s\d{3}\s\d{3}\s\d{3}|[+]48\s\d{2}\s\d{3}\s\d{2}\s\d{2}|[+]48\s\d{3}-\d{3}-\d{3}|12\s\d{3}\s\d{2}\s\d{2}'
		mail_pattern = re.compile(mail_regex)
		phone_pattern = re.compile(phone_regex)

		pretty_soup = soup.prettify()

		emails = mail_pattern.findall(pretty_soup)
		phones = phone_pattern.findall(pretty_soup)

		if len(emails) > 0:
			found['emails'] = True

		if len(phones) > 0:
			found['phones'] = True

		if (not found['emails'] or not found['phones']): # didn't found - let's try adding /kontakt in parentheses!
			if not added_kontakt:
				#print("Haven't found, searching in /kontakt.")
				url = url + "/kontakt"
				html = get_html_from_url(url)
				added_kontakt = True
				if html != "404":
					soup = BeautifulSoup(html, 'html.parser')
					continue
				else:
					if not added_html:
						#print("Haven't found, adding .html")
						url = url + ".html"
						html = get_html_from_url(url)
						if html != "404":
							soup = BeautifulSoup(html, 'html.parser')
						else:
							#print("Haven't found, searching through >a< tags on main page.") # find link with kontakt (a with href - if "kontakt" in href - go to that site and parse that)
							html = get_html_from_url(first_url)
							#print(first_url)
							soup = BeautifulSoup(html, 'html.parser')
							for a_tag in soup.find_all('a'):
								if ("contact" or "kontakt") in str(a_tag):
									try:
										html = get_html_from_url(a_tag.get('href'))
									except ValueError:
										#print("Couldn't find any.")
										return {'emails': emails, 'phones': phones, 'url': first_url}
									if html != "404":
										#print(a_tag.get('href'))
										soup = BeautifulSoup(html, 'html.parser')
										break
									else:
										break
							if not searched_through_a_tag:
								searched_through_a_tag = True
								continue
					else:
						#print("Haven't found any.")
						break
			else:
				#print("Haven't found any.")
				break

		else:
			print("Found.")
			return {'emails': remove_duplicates(emails), 'phones': remove_duplicates(phones), 'url': first_url}

	return {'emails': emails, 'phones': phones, 'url': first_url}

def remove_duplicates(list_of_duplicates):
	return list(dict.fromkeys(list_of_duplicates))

def save_search_results(link_list):
	with open(path_of_folder + "SearchResults.txt", 'w') as file:
		for x in link_list:
			file.write(x)
			file.write("\n")
		file.close()

def parse_through_sites(site_list):

	data_list = []
	counter = 0

	for site in site_list:
		#time.sleep(1)
		site_html = get_html_from_url(site)
		if site_html is not None:
			site_soup = BeautifulSoup(site_html, 'html.parser')
			data_list.append(parse_site(site_soup, site))

	return data_list

def make_folder():

	if not os.path.isdir(path_of_folder):
		os.mkdir(path_of_folder)

def write_data(data, search_name, extension=".csv"):

	if extension == ".txt":
		with open(path_of_folder + 'results.txt', 'w') as file:
			for dc in data:
				emails = dc['emails']
				phones = dc['phones']
				url = dc['url']

				if (emails and phones) != []: 
					file.write("Emails | Phones from {0}: {1} | {2}".format(url, emails, phones))
					file.write("\n")

			file.close()

	elif extension == ".csv":
		name = "results of +{0}+".format(search_name)
		with open(path_of_folder + name + ".csv", mode='w', newline='') as csv_file:
			fieldnames = ['site', 'emails', 'phones']
			writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
			writer.writeheader()

			for dc in data:
				emails = dc['emails']
				phones = dc['phones']
				url = dc['url']
				if (emails and phones) != []:
					writer.writerow({'site': url, 'emails': emails, 'phones': phones})

			csv_file.close()

def main():

	search = get_search_name()

	#quoted_search = "{0}{1}{2}".format(quote(search), quote(" "), quote(city))
	unquoted_search = "{0} {1}".format(search, city)

	links = simple_get_links(unquoted_search, 110)

	#sample_site = "http://akacjowydworkrakow.pl/"
	#sample_site_html = get_html_from_url(sample_site)

	#sample_soup = BeautifulSoup(sample_site_html, 'html.parser')
	#print(parse_site(sample_soup, sample_site))

	print("Finding emails and phones...")

	data = parse_through_sites(links)
	
	print("Writing file...")

	make_folder()
	write_data(data, unquoted_search)

	print("File written.")

	#save_search_results(links)
	time.sleep(3)

if __name__ == '__main__':
	main()


# https://python-googlesearch.readthedocs.io/en/latest/
# u niedzwiedzia: [10] http://www.uniedzwiedzia.pl/

# Algorithm:
# 1. Get into site
# 2. Try to find email and phone from the default site
# 3. If none (list is empty):
# 	a) try adding /kontakt to url
#		a) if none:
#			b) search "kontakt" on site and get href (link) of the site.
#		b) if 404, not found or any exception:
#			b) add .html
#			c) if still:
#				c)	add.php
